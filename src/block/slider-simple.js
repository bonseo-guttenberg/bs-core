const {__} = wp.i18n;
const {registerBlockType} = wp.blocks;
const {withSelect} = wp.data;
const BlockTitle = __('Slider Básico');
const BlockUrl = __('slider-basico');
import {CoreKeywords, Icons, CategoryGroup, EditorClass} from '../settings';
import {LoadingComponent} from '../services/ux';
import {BasicMaxEntries, BasicCta, TitleComponent, DescriptionComponent, CommonsElements} from "../services/basic";
import {PostTypeSelection} from '../services/selects';
import {PostTypes} from '../api/core';

registerBlockType('bonseo/block-bs-slider-simple', {
	title: BlockTitle,
	icon: Icons.slides,
	category: CategoryGroup,
	keywords: CoreKeywords,
	edit: withSelect((select) => {
		return {
			types: PostTypes(select),
		};
	})(function (props) {
		const {attributes, className, setAttributes} = props;
		if (!props.types) {
			return LoadingComponent();
		}
		return (
			<div className={EditorClass}>
				{TitleComponent(BlockTitle)}
				{DescriptionComponent(BlockUrl)}
				{BasicMaxEntries(className, attributes, setAttributes)}
				{BasicCta(className, attributes, setAttributes)}
				{PostTypeSelection(className, attributes, setAttributes, props.types)}
                {CommonsElements(className, attributes, setAttributes)}
			</div>
		);
	}),
	save: function () {
		return null;
	}
});
